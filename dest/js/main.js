$(document).ready(function() {
  $('.fancybox').fancybox();

  $('.tooltip').tooltipster({
    trigger:     'hover',
    animation:   'fade',
    interactive: true,
    theme:       'borderless'
  });

  $('form.form').submit(function(event) {
    sendAjaxForm(this, event);
  });
});

$(window).on('load', function() {
  $('.grid').masonry({
    columnWidth:     '.grid-sizer',
    itemSelector:    '.grid-item',
    percentPosition: true,
    gutter:          10,
    horizontalOrder: false
  });
});


function InitMainPage() {
  $('#fullpage').fullpage({
    menu:             '#menu, #menu-dots',
    scrollOverflow:   true,
    responsiveHeight: 500,
    responsiveWidth:  700,
    fixedElements:    '.header, .footer, .nav-dots',
    paddingTop:       '80px',
    paddingBottom:    '80px',
    navigation:       true
  });
}

function sendAjaxForm(form, event) {
  var fields = form.querySelectorAll('input, textarea')

  var formHasError =  Array.prototype.reduce.call(fields, function(invalidCount, currentItem) {
    if (currentItem.matches(':invalid')) invalidCount++;
  }, 0);

  if (formHasError) {
    return true;
  } else {
    event.preventDefault();

    var formData = new FormData(form);
    var xhr = new XMLHttpRequest();

    xhr.open('POST', 'send.php');
    xhr.onreadystatechange = function() {
      if ((xhr.readyState == 4) && (xhr.status == 200)) {
        data = xhr.responseText;
        form.outerHTML = '<h3 style="color: #000; text-align: center;">Спасибо, ваша заявка отправлена</h3><p style="text-align: center">Наши менеджеры свяжутся с вами в течение дня</p>';
      }
    }
    xhr.send(formData);

    return false;
  }
}